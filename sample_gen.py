import numpy as np
import numpy.random as rn

def gen_ab(n=10, p=0.0017, k=2, theta=2, gamma=1, mu_0=3):
    intervals = rn.geometric(p, n)
    cumsum = np.cumsum(intervals)
    t_true = cumsum[:(n - 1)] + 1

    sigma2_sample = [1/i for i in rn.gamma(shape = k/2, scale = theta/2, size = n)]
    mu_sample = [rn.normal(loc = mu_0, scale = np.sqrt(i/gamma), size = 1) for i in sigma2_sample]

    data = np.zeros(cumsum[n-1])
    for i in range(0, n):
        if i == 0:
            data[:cumsum[0]] = np.array(rn.normal(loc = mu_sample[0], scale = np.sqrt(sigma2_sample[0]), size = intervals[0]))
        else:
            data[cumsum[i - 1]:cumsum[i]] = np.array(rn.normal(loc = mu_sample[i], scale = np.sqrt(sigma2_sample[i]), size = intervals[i]))
    return data, t_true
